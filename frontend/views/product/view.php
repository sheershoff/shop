<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = $model->title;
$category = \common\models\Category::findOne($model->category_id);
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">
    <h1><?= Html::encode($this->title) ?>
        <small><?= isset($category) ? $category->title : 'No category' ?></small>
    </h1>
    <div class="row">
        <div class="col-md-5">
            <img class="img-responsive" src="http://placehold.it/350x300">
        </div>
        <div class="col-md-7">
            <p><?= Html::encode($model->description) ?></p>

            <h3><?= Html::encode($model->price) ?></h3>
        </div>
    </div>
</div>
